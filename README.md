**CoCoAnDa Distribution**

This repository contains distribution packages of CoCoAnDa.

Current version: 1.2.4

Please contact Friedemann Schwenkreis (friedemann.schwenkreis@dhbw-stuttgart.de) for further information regarding installation, setup and first steps.

